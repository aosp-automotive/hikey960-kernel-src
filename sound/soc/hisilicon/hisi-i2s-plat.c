/* SPDX-License-Identifier: GPL-2.0 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/delay.h>
#include <linux/clk.h>
#include <linux/jiffies.h>
#include <linux/io.h>
#include <linux/gpio.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/dmaengine_pcm.h>
#include <sound/initval.h>
#include <sound/soc.h>
#include <linux/interrupt.h>
#include <linux/reset.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
#include <linux/reset-controller.h>
#include <linux/clk.h>
#include <linux/regulator/consumer.h>
#include <sound/dmaengine_pcm.h>

static const struct snd_pcm_hardware snd_hisi_hardware = {
	.info	= SNDRV_PCM_INFO_MMAP |
				 SNDRV_PCM_INFO_MMAP_VALID |
				 SNDRV_PCM_INFO_PAUSE |
				 SNDRV_PCM_INFO_RESUME |
				 SNDRV_PCM_INFO_INTERLEAVED |
				 SNDRV_PCM_INFO_HALF_DUPLEX,
	.period_bytes_min	= 4096,
	.period_bytes_max	= 4096,
	.periods_min	= 4,
	.periods_max	= UINT_MAX,
	.buffer_bytes_max	= SIZE_MAX,
};

static const struct snd_dmaengine_pcm_config dmaengine_pcm_config = {
	.pcm_hardware	= &snd_hisi_hardware,
	.prepare_slave_config	= snd_dmaengine_pcm_prepare_slave_config,
	.prealloc_buffer_size	= 64 * 1024,
};

static int hisi_i2s_plat_probe(struct platform_device *pdev)
{
	int ret = 0;

	ret = devm_snd_dmaengine_pcm_register(&pdev->dev,
			&dmaengine_pcm_config,
			SND_DMAENGINE_PCM_FLAG_CUSTOM_CHANNEL_NAME);
	if (ret) {
		dev_err(&pdev->dev, "Failed to register dmaengine pcm\n");
		return -EINVAL;
	}
	return ret;
}

static int hisi_i2s_plat_remove(struct platform_device *pdev)
{
	return 0;
}

static const struct of_device_id hisi_i2s_plat_dt_ids[] = {
	{ .compatible = "hisilicon,hisi-i2s-plat"},
	{/* sentinel */ }
};

MODULE_DEVICE_TABLE(of, hisi_i2s_dt_ids);

static struct platform_driver hisi_i2s_plat_driver = {
	.probe = hisi_i2s_plat_probe,
	.remove = hisi_i2s_plat_remove,
	.driver = {
		.name = "hisi_i2s_plat",
		.owner = THIS_MODULE,
		.of_match_table = hisi_i2s_plat_dt_ids,
	},
};

module_platform_driver(hisi_i2s_plat_driver);

MODULE_DESCRIPTION("Hisilicon I2S Plat driver");
MODULE_AUTHOR("alik.hou");
MODULE_LICENSE("GPL");

